﻿using System;
using Grpc.Core;
using System.Collections.Generic;
using System.IO;
using GrpcCommon;
using GrpcExample;
using GrpcServer.Services;

namespace GrpcServer
{
    internal class Program
    {
        private static void Main()
        {
            var host = AppConfigs.Host;
            var httpPort = AppConfigs.HttpPort;
            var httpsPort = AppConfigs.HttpsPort;
            var cert = File.ReadAllText("Certs\\cert.pem");
            var key = File.ReadAllText("Certs\\key.pem");
            var server = new Server
            {
                Services =
                {
                    ExampleServer.BindService(new ExampleService())
                },
                Ports =
                {
                    new ServerPort(host, Convert.ToInt32(httpPort), ServerCredentials.Insecure),
                    new ServerPort(host, Convert.ToInt32(httpsPort), new SslServerCredentials(
                        new List<KeyCertificatePair>
                        {
                            new KeyCertificatePair(cert, key)
                        }))
                }
            };
            server.Start();

            Console.WriteLine($"Grpc Server Listening on http://{host}:{httpPort}, https://{host}:{httpsPort}");
            Console.ReadLine();

            server.ShutdownAsync().Wait();
        }
    }
}
