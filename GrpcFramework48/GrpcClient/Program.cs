﻿using System;
using GrpcClient.Test;

namespace GrpcClient
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // LongLived 测试
            //LongLivedUnit.Run();

            // Example 测试
            ExampleUnit.Run();


            Console.ReadKey();
        }
    }
}
