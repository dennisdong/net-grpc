﻿namespace GrpcCommon.Models
{
    /// <summary>
    /// Jwt Token
    /// </summary>
    public class JwtToken
    {
        /// <summary>
        /// 授权者
        /// </summary>
        public string userid { get; set; }

        /// <summary>
        /// Token过期时间
        /// </summary>
        public long exp { get; set; }

        /// <summary>
        /// Issuer
        /// </summary>
        public string iss { get; set; }
    }
}

