﻿using System;
using System.Configuration;

namespace GrpcCommon
{
    public class AppConfigs
    {
        public static string Host = GetValue("host");
        public static int HttpPort = Convert.ToInt32(GetValue("httpPort"));
        public static int HttpsPort = Convert.ToInt32(GetValue("httpsPort"));
        public static string Issuer = GetValue("issuer");
        public static int Expire = Convert.ToInt32(GetValue("expire"));
        public static string SecurityKey = GetValue("securityKey");


        public static string GetValue(string key)
        {
            try
            {
                return ConfigurationManager.AppSettings[key].Trim();
            }
            catch (Exception e)
            {
                throw new Exception($"AppConfig 配置获取异常,{e.StackTrace}");
            }
        }

        public static T GetValue<T>(string key) where T : class
        {
            try
            {
                return ConfigurationManager.AppSettings[key].Trim() as T;
            }
            catch (Exception e)
            {
                throw new Exception($"AppConfig 配置获取异常,{e.StackTrace}");
            }
        }
    }
}
