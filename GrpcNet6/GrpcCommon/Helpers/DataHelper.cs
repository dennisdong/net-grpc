﻿using System.Text;

namespace GrpcCommon.Helpers
{
    public class DataHelper
    {
        public static string RandomString(int length = 5)
        {
            char[] character = { '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'd', 'e', 'f', 'h', 'k', 'm', 'n', 'r', 'x', 'y', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'R', 'S', 'T', 'W', 'X', 'Y' };
            var text = new StringBuilder();
            var rnd = new Random();
            //生成验证码字符串 
            for (var i = 0; i < length; i++)
            {
                text.Append(character[rnd.Next(character.Length)]);
            }

            return text.ToString();
        }
    }
}
