﻿namespace GrpcCommon.Models
{
    public class UserDetails
    {
        public string? UserName { get; set; }
        public int Age { get; set; }
        public IEnumerable<string>? Friends { get; set; }
    }
}
