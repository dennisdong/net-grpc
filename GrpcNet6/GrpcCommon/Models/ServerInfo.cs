﻿namespace GrpcCommon.Models
{
    public class ServerInfo
    {
        public string? ID { get; set; }
        public string? Service { get; set; }
        public IEnumerable<string>? Tags { get; set; }
        public Meta? Meta { get; set; }
        public int Port { get; set; }
        public string? Address { get; set; }
        public Taggedaddresses? TaggedAddresses { get; set; }
        public Weights? Weights { get; set; }
        public bool EnableTagOverride { get; set; }
        public string? Datacenter { get; set; }
    }

    public class Meta
    {
    }

    public class Taggedaddresses
    {
        public LanIpv4? lan_ipv4 { get; set; }
        public WanIpv4? wan_ipv4 { get; set; }
    }

    public class LanIpv4
    {
        public string? Address { get; set; }
        public int Port { get; set; }
    }

    public class WanIpv4
    {
        public string? Address { get; set; }
        public int Port { get; set; }
    }

    public class Weights
    {
        public int Passing { get; set; }
        public int Warning { get; set; }
    }
}
