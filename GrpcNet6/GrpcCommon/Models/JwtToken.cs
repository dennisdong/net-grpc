﻿namespace GrpcCommon.Models
{
    public class JwtToken
    {
        public string? UserId { get; set; }
        public string? Exp { get; set; }
        public string? Iss { get; set; }
    }
}
