﻿using GrpcClient.Test;
using Microsoft.Extensions.Hosting;


// LongLived测试
//LongLivedTest.Run();

// Example测试
ExampleTest.Run();

Console.WriteLine("==================");
Console.WriteLine("按Ctrl+C停止程序");
Console.WriteLine("==================");

// 监听Ctrl+C
await new HostBuilder().RunConsoleAsync();