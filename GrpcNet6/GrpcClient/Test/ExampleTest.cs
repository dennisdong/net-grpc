﻿using System.Security.Cryptography.X509Certificates;
using Grpc.Net.Client;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using GrpcCommon.Helpers;
using GrpcExample;

namespace GrpcClient.Test
{
    internal class ExampleTest
    {
        public static void Run()
        {
            // 常规请求响应
            UnaryCall();

            // 服务器流响应
            StreamingFromServer();

            // 客户端流响应
            StreamingFromClient();

            // 双向流响应
            StreamingBothWays();
        }

        /// <summary>
        /// 创建客户端链接
        /// </summary>
        /// <param name="enableSsl"></param>
        /// <returns></returns>
        private static ExampleServer.ExampleServerClient CreateClient(bool enableSsl = true)
        {
            GrpcChannel channel;
            if (enableSsl)
            {
                const string serverUrl = "https://localhost:7000";
                Console.WriteLine($"尝试链接服务器,{serverUrl}");

                var handler = new HttpClientHandler();
                // 添加证书
                handler.ClientCertificates.Add(new X509Certificate2("Certs\\cert.pfx", "1234.com"));

                // 忽略证书
                handler.ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator;
                channel = GrpcChannel.ForAddress(serverUrl, new GrpcChannelOptions
                {
                    HttpClient = new HttpClient(handler)
                });
            }
            else
            {
                const string serverUrl = "http://localhost:5000";
                Console.WriteLine($"尝试链接服务器,{serverUrl}");
                channel = GrpcChannel.ForAddress(serverUrl);
            }

            Console.WriteLine("服务器链接成功");
            return new ExampleServer.ExampleServerClient(channel);
        }

        private static async void UnaryCall()
        {
            var client = CreateClient();
            const string securityKey = "Dennis!@#$%^123456.com";
            var userId = Guid.NewGuid().ToString();
            var token = JwtHelper.GenerateJwt(securityKey, userId);
            var result = await client.UnaryCallAsync(new ExampleRequest
            {
                SecurityKey = securityKey,
                UserId = "Dennis",
                UserDetail = new Struct
                {
                    Fields =
                    {
                        ["userName"] = Value.ForString("Dennis"),
                        ["age"] = Value.ForString("18"),
                        ["friends"] = Value.ForList(Value.ForString("Roger"), Value.ForString("YueBe"))
                    }
                },
                Token = token
            });
            Console.WriteLine($"Code={result.Code},Result={result.Result},Message={result.Message}");
        }

        private static async void StreamingFromServer()
        {
            var client = CreateClient();
            var result = client.StreamingFromServer(new ExampleRequest
            {
                UserId = "Dennis"
            });

            await foreach (var resp in result.ResponseStream.ReadAllAsync())
            {
                Console.WriteLine($"Code={resp.Code},Result={resp.Result},Message={resp.Message}");
            }
        }

        private static async void StreamingFromClient()
        {
            var client = CreateClient();
            var result = client.StreamingFromClient();

            // 发送请求
            for (var i = 0; i < 5; i++)
            {
                await result.RequestStream.WriteAsync(new ExampleRequest
                {
                    UserId = $"StreamingFromClient 第{i}次请求"
                });
                await Task.Delay(TimeSpan.FromSeconds(1));
            }

            // 等待请求发送完毕
            await result.RequestStream.CompleteAsync();

            var resp = result.ResponseAsync.Result;
            Console.WriteLine($"Code={resp.Code},Result={resp.Result},Message={resp.Message}");
        }

        private static async void StreamingBothWays()
        {
            var client = CreateClient();
            var result = client.StreamingBothWays();

            // 发送请求
            for (var i = 0; i < 5; i++)
            {
                await result.RequestStream.WriteAsync(new ExampleRequest
                {
                    UserId = $"StreamingBothWays 第{i}次请求"
                });
                await Task.Delay(TimeSpan.FromSeconds(1));
            }

            // 处理响应
            var respTask = Task.Run(async () =>
            {
                await foreach (var resp in result.ResponseStream.ReadAllAsync())
                {
                    Console.WriteLine($"Code={resp.Code},Result={resp.Result},Message={resp.Message}");
                }
            });

            // 等待请求发送完毕
            await result.RequestStream.CompleteAsync();

            // 等待响应处理
            await respTask;
        }
    }
}
